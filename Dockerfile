FROM ruby:2.7

RUN apt update -y && \
    apt upgrade -y

RUN mkdir -p /data/

WORKDIR /data

COPY ./src/entrypoint.bash /entrypoint.bash
COPY ./src/release.bash /release.bash

COPY ./app/ /data/

RUN chmod +x /release.bash && \
    chmod +x /entrypoint.bash && \
    gem install bundler && \
    bundle install && \
    bundle update

ENTRYPOINT [ "/entrypoint.bash" ]